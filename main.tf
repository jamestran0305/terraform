data "azurerm_client_config" "current" {
}
resource "azurerm_resource_group" "rg" {
  name     = "${var.project}_${var.region}"
  location = var.location
}
resource "azurerm_container_registry" "acr" {
  name                = "${var.project}acr"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "Standard"
  admin_enabled       = true
}
resource "azurerm_storage_account" "storage" {
  name                     = "${var.project}str"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
resource "azurerm_storage_container" "image_container" {
  name                  = "images"
  storage_account_name  = azurerm_storage_account.storage.name
  container_access_type = "private"
}

resource "azurerm_postgresql_server" "db_server" {
  name                = "${var.project}-psqlserver"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  administrator_login          = "psqladminun"
  administrator_login_password = "H@Sh1CoR3!"

  sku_name   = "GP_Gen5_4"
  version    = "9.6"
  storage_mb = 1048576

  ssl_enforcement_enabled          = true
  ssl_minimal_tls_version_enforced = "TLS1_2"
}

resource "azurerm_postgresql_database" "db" {
  name                = "${var.project}db"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_postgresql_server.db_server.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "azurerm_postgresql_database" "db_staging" {
  name                = "${var.project}db_staging"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_postgresql_server.db_server.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "azurerm_app_service_plan" "appplan" {
  name                = "${var.project}-appplan"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = var.appservice_tier
    size = var.appservice_size
  }
}
resource "azurerm_app_service" "backend_service" {
  name                = "${var.project}-backend"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.appplan.id

  site_config {
    linux_fx_version = "DOCKER|${azurerm_container_registry.acr.name}.azurecr.io/printgrows/backend:latest"
    cors {
      allowed_origins = ["*"]
    }
  }

  app_settings = {
    "DOCKER_ENABLE_CI"                = true
    "DOCKER_REGISTRY_SERVER_USERNAME" = azurerm_container_registry.acr.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD" = azurerm_container_registry.acr.admin_password
    "DOCKER_REGISTRY_SERVER_URL"      = azurerm_container_registry.acr.login_server
    "SENDGRID_API_KEY"                = ""
    "TYPEORM_CONNECTION"              = "postgres"
    "TYPEORM_HOST"                    = "${azurerm_postgresql_server.db_server.name}.postgres.database.azure.com"
    "TYPEORM_USERNAME"                = "${azurerm_postgresql_server.db_server.administrator_login}@${azurerm_postgresql_server.db_server.name}"
    "TYPEORM_PASSWORD"                = azurerm_postgresql_server.db_server.administrator_login_password
    "TYPEORM_DATABASE"                = azurerm_postgresql_database.db.name
    "TYPEORM_PORT"                    = "5432"
    "TYPEORM_SYNCHRONIZE"             = true
    "TYPEORM_LOGGING"                 = true
    "TYPEORM_ENTITIES"                = "src/entity/**/*.ts"
    "TYPEORM_MIGRATIONS"              = "src/migration/**/*.ts"
    "TYPEORM_SUBSCRIBERS"             = "src/subscriber/**/*.ts"
    "STORAGE_ACCOUNT_NAME"            = azurerm_storage_account.storage.name
    "STORAGE_ACCOUNT_SAS"             = azurerm_storage_account.storage.primary_access_key
    "STORAGE_ACCOUNT_CONTAINER"       = azurerm_storage_container.image_container.name
    "WEBSITES_PORT"                   = 4000
  }
}

resource "azurerm_app_service_slot" "backend_service_staging" {
  name                = "${var.project}-backend-staging"
  app_service_name    = azurerm_app_service.backend_service.name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.appplan.id

  site_config {
    linux_fx_version = "DOCKER|${azurerm_container_registry.acr.name}.azurecr.io/printgrows/backend-staging:latest"
    cors {
      allowed_origins = ["*"]
    }
  }

  app_settings = {
    "DOCKER_ENABLE_CI"                = true
    "DOCKER_REGISTRY_SERVER_USERNAME" = azurerm_container_registry.acr.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD" = azurerm_container_registry.acr.admin_password
    "DOCKER_REGISTRY_SERVER_URL"      = azurerm_container_registry.acr.login_server
    "SENDGRID_API_KEY"                = ""
    "TYPEORM_CONNECTION"              = "postgres"
    "TYPEORM_HOST"                    = "${azurerm_postgresql_server.db_server.name}.postgres.database.azure.com"
    "TYPEORM_USERNAME"                = "${azurerm_postgresql_server.db_server.administrator_login}@${azurerm_postgresql_server.db_server.name}"
    "TYPEORM_PASSWORD"                = azurerm_postgresql_server.db_server.administrator_login_password
    "TYPEORM_DATABASE"                = azurerm_postgresql_database.db_staging.name
    "TYPEORM_PORT"                    = "5432"
    "TYPEORM_SYNCHRONIZE"             = true
    "TYPEORM_LOGGING"                 = true
    "TYPEORM_ENTITIES"                = "src/entity/**/*.ts"
    "TYPEORM_MIGRATIONS"              = "src/migration/**/*.ts"
    "TYPEORM_SUBSCRIBERS"             = "src/subscriber/**/*.ts"
    "STORAGE_ACCOUNT_NAME"            = azurerm_storage_account.storage.name
    "STORAGE_ACCOUNT_SAS"             = azurerm_storage_account.storage.primary_access_key
    "STORAGE_ACCOUNT_CONTAINER"       = azurerm_storage_container.image_container.name
    "WEBSITES_PORT"                   = 4000
  }
}

resource "azurerm_app_service" "fontend_service" {
  name                = "${var.project}-frontend"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.appplan.id

  site_config {
    linux_fx_version = "DOCKER|${azurerm_container_registry.acr.name}.azurecr.io/printgrows/frontend:latest"
  }

  app_settings = {
    "DOCKER_ENABLE_CI"                = true
    "DOCKER_REGISTRY_SERVER_USERNAME" = azurerm_container_registry.acr.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD" = azurerm_container_registry.acr.admin_password
    "DOCKER_REGISTRY_SERVER_URL"      = azurerm_container_registry.acr.login_server
    "API_ENDPOINT"                    = "https://${azurerm_app_service.backend_service.default_site_hostname}/api"
    "WEBSITES_PORT"                   = 3000
  }
}

resource "azurerm_app_service_slot" "frontend_service_staging" {
  name                = "${var.project}-frontend-staging"
  app_service_name    = azurerm_app_service.fontend_service.name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.appplan.id

  site_config {
    linux_fx_version = "DOCKER|${azurerm_container_registry.acr.name}.azurecr.io//printgrows/frontend-staging:latest"
  }

  app_settings = {
    "DOCKER_ENABLE_CI"                = true
    "DOCKER_REGISTRY_SERVER_USERNAME" = azurerm_container_registry.acr.admin_username
    "DOCKER_REGISTRY_SERVER_PASSWORD" = azurerm_container_registry.acr.admin_password
    "DOCKER_REGISTRY_SERVER_URL"      = azurerm_container_registry.acr.login_server
    "API_ENDPOINT"                    = "https://${azurerm_app_service_slot.backend_service_staging.default_site_hostname}/api"
    "WEBSITES_PORT"                   = 3000
  }
}
