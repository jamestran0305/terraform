output "acr_id" {
  value = azurerm_container_registry.acr.id
}

output "acr_admin_username" {
  value = azurerm_container_registry.acr.admin_username
}

output "acr_admin_password" {
  value = azurerm_container_registry.acr.admin_password
  sensitive = true
}

output "backend_service_name" {
    value = azurerm_app_service.backend_service.name
}

output "backend_service_default_hostname" {
    value = azurerm_app_service.backend_service.default_site_hostname
}
