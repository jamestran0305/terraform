terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.17"
    }
    # Needed for the traffic manager role assignment
    azuread = {
      source  = "hashicorp/azuread"
      version = ">= 0.7"
    }
  }
}

provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}

terraform {
  backend "http" {

  }
}
