variable "project" {
  description = "(Required) Project name (ex. ahomely)"
  default     = "printgrows"
}
variable "region" {
  description = "(Required) Region name (ex. southeastasia)"
  default     = "apac"
}
variable "location" {
  description = "(Required) Specifies the supported Azure location where the resource exists. Changing this forces a new resource to be created."
  default     = "Southeast Asia"
}

variable "appservice_tier" {
  default = "Premium"
}

variable "appservice_size" {
  default = "P1v3"
}

